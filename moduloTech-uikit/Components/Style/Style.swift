//
//  Style.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

public enum Style {

    enum Color {
        static let backgroundColor = UIColor(rgb: 0x2F3745)
        static let cellBackgroundColor = UIColor.white
        static let cellSeparatorColor = UIColor(rgb: 0xA7A79C)
        static let titleColor = UIColor(rgb: 0xe09d38)
        static let clear = UIColor.clear
    }

    enum Font {
        static func mainRegular(size: CGFloat) -> UIFont {
            UIFont(name: "SFProText-Regular", size: size)!
        }

        static func mainMedium(size: CGFloat) -> UIFont {
            UIFont(name: "SFProText-Medium", size: size)!
        }

        static func mainBold(size: CGFloat) -> UIFont {
            UIFont(name: "SFProText-Bold", size: size)!
        }

        static func mainSemibold(size: CGFloat) -> UIFont {
            UIFont(name: "SFProText-Semibold", size: size)!
        }

        static let header = mainSemibold(size: 20.0)
        static let cellTitle = mainMedium(size: 17.0)
        static let cellSubTitle = mainRegular(size: 15.0)
    }

    enum CornerRadius {
        static let card: CGFloat = 12.0
        static let button: CGFloat = 6.0
        static let smallButton: CGFloat = 3.0
    }
    
    enum ContentInsets {
        static let cellInsets = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 0.0)
    }

}
