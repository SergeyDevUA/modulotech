//
//  Network.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 27.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class RequestObservable {

	private lazy var jsonDecoder = JSONDecoder()
	private var urlSession: URLSession

	public init(config:URLSessionConfiguration) {
		let configuration = URLSessionConfiguration.default
		urlSession = URLSession(configuration: configuration)
	}

	public func callAPI<ItemModel: Decodable>(request: URLRequest) -> Observable<ItemModel> {

		return Observable.create { observer in

			let task = self.urlSession.dataTask(with: request) { (data, response, error) in
				if let httpResponse = response as? HTTPURLResponse{
					let statusCode = httpResponse.statusCode
					do {
						let _data = data ?? Data()
						if (200...399).contains(statusCode) {
							let objs = try self.jsonDecoder.decode(ItemModel.self, from: _data)
							print(objs)
							observer.onNext(objs)
						}
						else {
							observer.onError(error!)
						}
					} catch {
						observer.onError(error)
					}
				}
				observer.onCompleted()
			}
			task.resume()
			return Disposables.create {
				task.cancel()
			}
		}
	}

}

class APIClient {

	private let domain = "http://storage42.com"
	static var shared = APIClient()
	lazy var requestObservable = RequestObservable(config: .default)
	
	func fetchModulotestData() throws -> Observable<HomeModel> {
		let url = URL(string: "\(domain)/modulotest/data")!
		var request = URLRequest(url: url)
		request.httpMethod = "GET"
		return requestObservable.callAPI(request: request)
	}

}
