//
//  View.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 02.03.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

extension UIView {

	var safeTopAnchor: NSLayoutYAxisAnchor {
		if #available(iOS 11.0, *) {
			return self.safeAreaLayoutGuide.topAnchor
		}
		return self.topAnchor
	}

	var safeLeftAnchor: NSLayoutXAxisAnchor {
		if #available(iOS 11.0, *){
			return self.safeAreaLayoutGuide.leftAnchor
		}
		return self.leftAnchor
	}

	var safeRightAnchor: NSLayoutXAxisAnchor {
		if #available(iOS 11.0, *){
			return self.safeAreaLayoutGuide.rightAnchor
		}
		return self.rightAnchor
	}

	var safeBottomAnchor: NSLayoutYAxisAnchor {
		if #available(iOS 11.0, *) {
			return self.safeAreaLayoutGuide.bottomAnchor
		}
		return self.bottomAnchor
	}

}
