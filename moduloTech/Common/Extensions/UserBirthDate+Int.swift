//
//  Int.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 03.03.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation

extension Int {

	func formattedUserBirthIntToString() -> String {
		let timeInterval = Double(self / 1000)
		let date = Date(timeIntervalSince1970: timeInterval)
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		return dateFormatter.string(from: date)
	}

}
