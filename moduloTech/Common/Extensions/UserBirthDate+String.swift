//
//  UserBirthDate+String.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 04.03.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation

extension String {

	func formattedUserBirthStringToInt() -> Int {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let date = dateFormatter.date(from: self)
		let timeInterval = date?.timeIntervalSince1970 ?? 0
		return Int(timeInterval * 1000)
	}

}
