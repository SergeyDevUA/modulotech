//
//  Color.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

extension UIColor {

	convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
		self.init(
			red: CGFloat(red) / 255.0,
			green: CGFloat(green) / 255.0,
			blue: CGFloat(blue) / 255.0,
			alpha: a
		)
	}

	convenience init(rgb: Int, a: CGFloat = 1.0) {
		self.init(
			red: (rgb >> 16) & 0xFF,
			green: (rgb >> 8) & 0xFF,
			blue: rgb & 0xFF,
			a: a
		)
	}

}

