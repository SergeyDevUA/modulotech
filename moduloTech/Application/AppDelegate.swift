//
//  AppDelegate.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 25.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

	let rootViewControler = BootstrapViewController()
	let navigationController = UINavigationController(rootViewController: rootViewControler)
	navigationController.navigationBar.isHidden = true
	
	window = UIWindow(frame: UIScreen.main.bounds)
	window?.rootViewController = navigationController
	window?.makeKeyAndVisible()

	return true
}

}

