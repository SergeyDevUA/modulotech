//
//  BaseViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 03.03.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

	// Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Main view
		self.view.backgroundColor = Style.Color.backgroundColor
	}

}
