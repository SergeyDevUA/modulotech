//
//  FilterViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class FilterViewController: BaseViewController {
	
	private var headerView: UIView!
	private var filtersTableView: UITableView!
	private var doneButton: UIButton!
	private var viewModel: FilterViewModel?
	private let cellReuseIdentifier = "FilterTableViewCell"
	private let filters: [String] = ["Light", "Heater", "RollerShutter"]

	// Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupUI()
	}

}

extension FilterViewController {

	static func build(slug: String) -> FilterViewController {
		let viewController: FilterViewController = FilterViewController()
		viewController.viewModel = FilterViewModel(filters: [""])
		return viewController
	}

}

private extension FilterViewController {
	
	func setupUI() {
		setupHeader()
		setupTableView()
	}
	
	func setupHeader() {
		// Header container
		headerView = UIView()
		headerView.backgroundColor = Style.Color.backgroundColor
		self.view.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		headerView.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
		headerView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
		headerView.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
		
		// Title label
		let titleLabel = UILabel()
		titleLabel.text = NSLocalizedString("filter_title")
		titleLabel.textAlignment = .center
		titleLabel.font = Style.Font.header
		titleLabel.textColor = .white
		headerView.addSubview(titleLabel)
		
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
		titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
		titleLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
		
		// Close button
		let closeButton = UIButton(type: .custom)
		if let image = UIImage(named: "close.png") {
			closeButton.setImage(image, for: .normal)
		}
		closeButton.addTarget(self, action: #selector(closeFilterAction(_:)), for: .touchUpInside)
		headerView.addSubview(closeButton)
		
		closeButton.translatesAutoresizingMaskIntoConstraints = false
		closeButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		closeButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0).isActive = true
		closeButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		closeButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
		
		// Done button
		doneButton = UIButton(type: .custom)
		doneButton.isHidden = true
		if let image = UIImage(named: "done.png") {
			doneButton.setImage(image, for: .normal)
		}
		doneButton.addTarget(self, action: #selector(doneAction(_:)), for: .touchUpInside)
		headerView.addSubview(doneButton)

		doneButton.translatesAutoresizingMaskIntoConstraints = false
		doneButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		doneButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor,constant: -16.0).isActive = true
		doneButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		doneButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
	}
	
	func setupTableView() {
		let frame = self.view.frame
		filtersTableView = UITableView(frame: frame)
		filtersTableView.separatorStyle = .none
		self.view.addSubview(filtersTableView)
		
		filtersTableView.translatesAutoresizingMaskIntoConstraints = false
		filtersTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		filtersTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
		filtersTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
		filtersTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
	
		filtersTableView.register(FilterTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		filtersTableView.delegate = self
		filtersTableView.dataSource = self
	}
	
	@objc func closeFilterAction(_ sender: UIButton?) {
		dismiss(animated: true, completion: nil)
	}
	
	@objc func doneAction(_ sender: UIButton?) {
		dismiss(animated: true, completion: nil)
	}

}

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filters.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let item = indexPath.item
		let cell = filtersTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)  as! FilterTableViewCell
		cell.titleLabel.text = filters[item]
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60.0
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		doneButton.isHidden = false
		
		// TODO: Apply search
	}

}

