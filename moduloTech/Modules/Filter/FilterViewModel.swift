//
//  FilterViewModel.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 28.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation
import RxSwift

final class FilterViewModel {

	private var filters: [String] = [String]()

	init(filters: [String]) {
		self.filters = filters
	}

}
