//
//  FilterTableViewCell.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
	
	var titleLabel: UILabel!
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		initUI()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func initUI() {
		// Main view
		self.contentView.backgroundColor = .white

		// Title label
		titleLabel = UILabel()
		titleLabel.textAlignment = .left
		titleLabel.font = Style.Font.cellTitle
		titleLabel.textColor = Style.Color.backgroundColor
		contentView.addSubview(titleLabel)

		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0).isActive = true
		titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		titleLabel.widthAnchor.constraint(equalToConstant: 120.0).isActive = true
		titleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 1.0).isActive = true

		//	Separator
		let bottomSeparatorView = UIView()
		bottomSeparatorView.backgroundColor = Style.Color.cellSeparatorColor
		bottomSeparatorView.alpha = 0.3
		contentView.addSubview(bottomSeparatorView)
		
		bottomSeparatorView.translatesAutoresizingMaskIntoConstraints = false
		bottomSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
		bottomSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
		bottomSeparatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
	}

}
