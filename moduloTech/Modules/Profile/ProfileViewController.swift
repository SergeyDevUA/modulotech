//
//  ProfileViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

enum ProfileTypes {
	case firstName
	case lastName
	case city
	case postalCode
	case street
	case streetCode
	case country
	case birthDate
	case other
}

struct ProfileItem {
	let title: String
	let value: String
	let type: ProfileTypes
}

class ProfileViewController: BaseViewController {
	
	private var headerView: UIView!
	private var profileTableView: UITableView!
	private var doneButton: UIButton!
	private var viewModel: ProfileViewModel?
	private let cellReuseIdentifier = "ProfileTableViewCell"

	// Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupUI()
	}

}

extension ProfileViewController {

	static func build(user: User) -> ProfileViewController {
		let viewController: ProfileViewController = ProfileViewController()
		let userItems: [ProfileItem] = [
			ProfileItem(title: NSLocalizedString("profile_firstName"), value: user.firstName, type: .firstName),
			ProfileItem(title: NSLocalizedString("profile_lastName"), value: user.lastName, type: .lastName),
			ProfileItem(title: NSLocalizedString("profile_city"), value: user.address.city, type: .city),
			ProfileItem(title: NSLocalizedString("profile_postalCode"), value: String(user.address.postalCode), type: .postalCode),
			ProfileItem(title: NSLocalizedString("profile_street"), value: user.address.street, type: .street),
			ProfileItem(title: NSLocalizedString("profile_streetCode"), value: user.address.streetCode, type: .streetCode),
			ProfileItem(title: NSLocalizedString("profile_country"), value: user.address.country, type: .country),
			ProfileItem(title: NSLocalizedString("profile_birthDate"), value: user.birthDate.formattedUserBirthIntToString(), type: .birthDate)
		]
		viewController.viewModel = ProfileViewModel(userItems: userItems, user: user)
		return viewController
	}

}

private extension ProfileViewController {
	
	func setupUI() {
		setupHeader()
		setupTableView()
	}
	
	func setupHeader() {
		// Header container
		headerView = UIView()
		headerView.backgroundColor = Style.Color.backgroundColor
		self.view.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		headerView.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
		headerView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
		headerView.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
		
		// Title label
		let titleLabel = UILabel()
		titleLabel.text = NSLocalizedString("profile_title")
		titleLabel.textAlignment = .center
		titleLabel.font = Style.Font.header
		titleLabel.textColor = .white
		headerView.addSubview(titleLabel)
		
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
		titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
		titleLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
		
		// Close button
		let closeButton = UIButton(type: .custom)
		if let image = UIImage(named: "close.png") {
			closeButton.setImage(image, for: .normal)
		}
		closeButton.addTarget(self, action: #selector(closeFilterAction(_:)), for: .touchUpInside)
		headerView.addSubview(closeButton)
		
		closeButton.translatesAutoresizingMaskIntoConstraints = false
		closeButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		closeButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0).isActive = true
		closeButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		closeButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
		
		// Done button
		doneButton = UIButton(type: .custom)
		doneButton.isHidden = true
		if let image = UIImage(named: "done.png") {
			doneButton.setImage(image, for: .normal)
		}
		doneButton.addTarget(self, action: #selector(doneAction(_:)), for: .touchUpInside)
		headerView.addSubview(doneButton)

		doneButton.translatesAutoresizingMaskIntoConstraints = false
		doneButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		doneButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0).isActive = true
		doneButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		doneButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
	}
	
	func setupTableView() {
		let frame = self.view.frame
		profileTableView = UITableView(frame: frame)
		profileTableView.separatorStyle = .none
		self.view.addSubview(profileTableView)
			
		profileTableView.translatesAutoresizingMaskIntoConstraints = false
		profileTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		profileTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
		profileTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
		profileTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
		
		profileTableView.register(ProfileTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		profileTableView.delegate = self
		profileTableView.dataSource = self
	}
	
	@objc func closeFilterAction(_ sender: UIButton?) {
		backToDeviceList()
	}
	
	@objc func doneAction(_ sender: UIButton?) {
		backToDeviceList()
	}

	func backToDeviceList() {
		self.view.endEditing(true)
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
			self.dismiss(animated: true, completion: nil)
		}
	}

}

extension ProfileViewController: ProfileTableViewCellProtocol {

	func dataWasChange(type: ProfileTypes, date: String) {
		if doneButton.isHidden {
			doneButton.isHidden = false
		}
		switch type {
			case .firstName:
				viewModel?.user?.firstName = date
			case .lastName:
				viewModel?.user?.lastName = date
			case .city:
				viewModel?.user?.address.city = date
			case .postalCode:
				viewModel?.user?.address.postalCode = Int(date) ?? 0
			case .street:
				viewModel?.user?.address.street = date
			case .streetCode:
				viewModel?.user?.address.streetCode = date
			case .country:
				viewModel?.user?.address.country = date
			case .birthDate:
				viewModel?.user?.birthDate = date.formattedUserBirthStringToInt()
			case .other:
				print("other type")
		}
	}

}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let userCount = viewModel?.userItems.count else { return 0 }

		return userCount
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = profileTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)  as! ProfileTableViewCell
		let item = indexPath.item
		if let userData = viewModel?.userItems[item] {
			cell.delegate = self
			cell.titleLabel.text = userData.title
			cell.textFiled.text = userData.value
			cell.itemType = userData.type
		}
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60.0
	}
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
}

