//
//  ProfileViewModel.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 28.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation
import RxSwift

final class ProfileViewModel {

	let user: User?
	let userItems: [ProfileItem]
	
	init(userItems: [ProfileItem], user: User) {
        self.userItems = userItems
		self.user = user
    }

}
