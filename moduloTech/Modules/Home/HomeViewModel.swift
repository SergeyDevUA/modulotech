//
//  HomeViewModel.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 27.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation
import RxSwift

final class HomeViewModel {

	private let client = APIClient.shared
	private let bag = DisposeBag()
	var user: User?
	let devices = PublishSubject<[Devices]>()
	
	func loadData() {
		do {
			let homeData = try client.fetchModulotestData()
			homeData
				.map({ $0.devices })
				.subscribe(onNext: { devices in
					self.devices.onNext(devices)
					self.devices.onCompleted()
				}).disposed(by: bag)
			homeData
				.map({ $0.user })
				.subscribe(onNext: { user in
					self.user = user
				}).disposed(by: bag)
		} catch {
			print("func loadData is failed")
		}
	}

}
