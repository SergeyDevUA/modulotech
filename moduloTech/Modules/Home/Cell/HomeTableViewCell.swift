//
//  HomeTableViewCell.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
	
	private var titleLabel: UILabel!
	private var percentageLabel: UILabel!
	private var statusImageView: UIImageView!
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		initUI()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setupCell(device: Devices) {
		titleLabel.text = device.deviceName
		var activeStatusImage: UIImage?
		var noActiveStatusImage: UIImage?
		switch device.productType {
			case .light:
				activeStatusImage = UIImage(named: "light_on.png")
				noActiveStatusImage = UIImage(named: "light_off.png")
				if let intensity = device.intensity {
					percentageLabel.text = String(intensity) + "%"
				}
			case .heater:
				activeStatusImage = UIImage(named: "temperature_on.png")
				noActiveStatusImage = UIImage(named: "temperature_off.png")
				if let temperature = device.temperature {
					percentageLabel.text = String(temperature) + "%"
				}
			case .rollerShutter:
				// RollerShutter with out mode, it ON all time!
				activeStatusImage = UIImage(named: "window_on.png")
				noActiveStatusImage = UIImage(named: "window_on.png")
				if let position = device.position {
					percentageLabel.text = String(position) + "%"
				}
			case .otherDevice:
				activeStatusImage = UIImage(named: "home_on.png")
				noActiveStatusImage = UIImage(named: "home_off.png")
		}
		statusImageView.image = device.mode ? activeStatusImage : noActiveStatusImage
	}

	func initUI() {
		//	Cell
		contentView.backgroundColor = Style.Color.cellBackgroundColor
		
		//	Title
		titleLabel = UILabel()
		titleLabel.textAlignment = .left
		titleLabel.font = Style.Font.cellTitle
		titleLabel.numberOfLines = 0
		titleLabel.lineBreakMode = .byWordWrapping
		contentView.addSubview(titleLabel)

		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0).isActive = true
		titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.65).isActive = true
		titleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 1.0).isActive = true
		
		//	Percentage of use
		percentageLabel = UILabel()
		percentageLabel.font = Style.Font.cellSubTitle
		percentageLabel.textAlignment = .left
		contentView.addSubview(percentageLabel)

		percentageLabel.translatesAutoresizingMaskIntoConstraints = false
		percentageLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:  -65.0).isActive = true
		percentageLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19.0).isActive = true
		percentageLabel.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
		percentageLabel.heightAnchor.constraint(equalToConstant: 27.0).isActive = true

		//	Status
		statusImageView = UIImageView()
		contentView.addSubview(statusImageView)
		
		statusImageView.translatesAutoresizingMaskIntoConstraints = false
		statusImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:  -25.0).isActive = true
		statusImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19.0).isActive = true
		statusImageView.widthAnchor.constraint(equalToConstant: 27.0).isActive = true
		statusImageView.heightAnchor.constraint(equalToConstant: 27.0).isActive = true

		//	Separator
		let bottomSeparatorView = UIView()
		bottomSeparatorView.backgroundColor = Style.Color.cellSeparatorColor
		bottomSeparatorView.alpha = 0.3
		contentView.addSubview(bottomSeparatorView)
		
		bottomSeparatorView.translatesAutoresizingMaskIntoConstraints = false
		bottomSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
		bottomSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
		bottomSeparatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
	}

}

