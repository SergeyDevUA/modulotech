//
//  HomeViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 25.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: BaseViewController , UIScrollViewDelegate {

	private var headerView: UIView!
	private var devicesTableView: UITableView!
	private let refreshControll = UIRefreshControl()
	private let bag = DisposeBag()
	private var viewModel: HomeViewModel?
	private let cellReuseIdentifier = "HomeTableViewCell"

	//	Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setup()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		devicesTableView.reloadData()
	}

}

extension HomeViewController {

	static func build() -> HomeViewController {
		let viewController: HomeViewController = HomeViewController()
		viewController.viewModel = HomeViewModel()
		return viewController
	}

}

private extension HomeViewController {

	func setup() {
		setupHeader()
		setupTableViewAndBinding()
	}
	
	func setupHeader() {
		//	Header view container
		headerView = UIView()
		self.view.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		headerView.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
		headerView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
		headerView.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
		
		//	Title label
		let titleLabel = UILabel()
		titleLabel.text = NSLocalizedString("home_title")
		titleLabel.textAlignment = .center
		titleLabel.font = Style.Font.header
		titleLabel.textColor = .white
		headerView.addSubview(titleLabel)
		
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
		titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
		
		//	User button
		let profileButton = UIButton(type: .custom)
		if let image = UIImage(named: "user.png") {
			profileButton.setImage(image, for: .normal)
		}
		profileButton.addTarget(self, action: #selector(profileAction(_:)), for: .touchUpInside)
		headerView.addSubview(profileButton)
		
		profileButton.translatesAutoresizingMaskIntoConstraints = false
		profileButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		profileButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0).isActive = true
		profileButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		profileButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
		
		//	Filter button
		let filterButton = UIButton(type: .custom)
		if let image = UIImage(named: "filter.png") {
			filterButton.setImage(image, for: .normal)
		}
		filterButton.addTarget(self, action: #selector(filterAction(_:)), for: .touchUpInside)
		headerView.addSubview(filterButton)

		filterButton.translatesAutoresizingMaskIntoConstraints = false
		filterButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		filterButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor,constant: -16.0).isActive = true
		filterButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		filterButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
	}
	
	func setupTableViewAndBinding() {
		//	TableVeiew init
		let frame = self.view.frame
		devicesTableView = UITableView(frame: frame)
		devicesTableView.separatorStyle = .none
		devicesTableView.backgroundColor = Style.Color.cellBackgroundColor
		self.view.addSubview(devicesTableView)
		
		devicesTableView.translatesAutoresizingMaskIntoConstraints = false
		devicesTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		devicesTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
		devicesTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
		devicesTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
	
		devicesTableView.register(HomeTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		
		//	TableVeiew binding
		devicesTableView.rx.setDelegate(self).disposed(by: bag)

		devicesTableView.rx.modelSelected(Devices.self).subscribe { [weak self] model in
			guard let devices = model.element else { return }
			guard let nav = self?.navigationController else { return }

			switch devices.productType {
				case .light:
					let lightDeviceTypeViewController = LightDeviceTypeViewController.build(devices: devices)
					nav.pushViewController(lightDeviceTypeViewController, animated: true)
				case .heater:
					let heaterDeviceTypeViewController = HeaterDeviceTypeViewController.build(devices: devices)
					nav.pushViewController(heaterDeviceTypeViewController, animated: true)
				case .rollerShutter:
					let rollerShutterDeviceTypeViewController = RollerShutterDeviceTypeViewController.build(devices: devices)
					nav.pushViewController(rollerShutterDeviceTypeViewController, animated: true)
				case .otherDevice:
					print("unknown device type")
			}
		}.disposed(by: bag)
		devicesTableView.rx.itemDeleted
		.subscribe {
			print($0)
		}.disposed(by: bag)

		//	ViewModel binding
		viewModel?.devices.bind(to: devicesTableView.rx.items(cellIdentifier: cellReuseIdentifier,
															  cellType: HomeTableViewCell.self)) { row, item, cell in
			cell.selectionStyle = .none
			cell.setupCell(device: item)
		}.disposed(by: bag)

		viewModel?.loadData()
	}
	
	//	Actions
	@objc func filterAction(_ sender: UIButton?) {
		let filterViewController = FilterViewController.build(slug: "")
		filterViewController.modalPresentationStyle = .fullScreen
		present(filterViewController, animated: true, completion: nil)
	}
	
	@objc func profileAction(_ sender: UIButton?) {
		guard let user = viewModel?.user else { return }

		let profileViewController = ProfileViewController.build(user: user)
		profileViewController.modalPresentationStyle = .fullScreen
		present(profileViewController, animated: true, completion: nil)
	}
	
}

extension HomeViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 64.0
	}

}
