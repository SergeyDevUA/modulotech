//
//  HomeModel.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 28.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation

enum ProductType {
	case light
	case heater
	case rollerShutter
	case otherDevice
}

class HomeModel: Decodable {

	let devices: [Devices]
	let user: User

}

class Devices: Decodable {
	
	let id: Int
	var deviceName: String
	var intensity: Int?
	var mode: Bool = false
	var position: Int?
	var temperature: Int?
	var productType: ProductType = .otherDevice
	
	enum CodingKeys: String, CodingKey {
		case id
		case deviceName
		case intensity
		case mode
		case position
		case temperature
		case productType
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		id = try container.decode(Int.self, forKey: .id)
		deviceName = try container.decode(String.self, forKey: .deviceName)
		intensity = try? container.decode(Int?.self, forKey: .intensity)
		let status = try? container.decode(String?.self, forKey: .mode)
		if let status = status, status == "ON" {
			mode = true
		}
		position = try? container.decode(Int?.self, forKey: .position)
		temperature = try? container.decode(Int?.self, forKey: .temperature)
		let prodType = try container.decode(String.self, forKey: .productType)
		switch prodType {
			case "Light": productType = .light
			case "Heater": productType = .heater
			case "RollerShutter": productType = .rollerShutter
		default: break
		}
	}
	
}

class User: Decodable {

	var firstName: String
	var lastName: String
	var address: UserAddress
	var birthDate: Int

}

class UserAddress: Decodable {

	var city: String
	var postalCode: Int
	var street: String
	var streetCode: String
	var country: String

}
