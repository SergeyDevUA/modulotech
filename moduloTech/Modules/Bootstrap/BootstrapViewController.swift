//
//  BootstrapViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class BootstrapViewController: UIViewController {
	
	private var bodyView: UIView!
	private var logoImageView: UIImageView!
	private var scale: CGFloat = 1.0

	//	Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupUI()
	}

}

private extension BootstrapViewController {

	func setupUI() {
		setupNavigationBar()
		setupBody()
		setupLogoImage()
	}
	
	func setupNavigationBar() {
		navigationController?.navigationBar.isHidden = true
	}
	
	func setupBody() {
		let frame = self.view.frame
		bodyView = UIView(frame: frame)
		bodyView.backgroundColor = Style.Color.backgroundColor
		self.view.addSubview(bodyView)
		
		bodyView.translatesAutoresizingMaskIntoConstraints = false
		bodyView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		bodyView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
		bodyView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
		bodyView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
	}
	
	func setupLogoImage() {
		let image = UIImage(named: "logo")
		logoImageView = UIImageView(image: image)
		bodyView.addSubview(logoImageView)
		
		logoImageView.translatesAutoresizingMaskIntoConstraints = false
		logoImageView.centerXAnchor.constraint(equalTo: bodyView.centerXAnchor).isActive = true
		logoImageView.centerYAnchor.constraint(equalTo: bodyView.centerYAnchor).isActive = true
		logoImageView.widthAnchor.constraint(equalTo: bodyView.widthAnchor, multiplier: 0.64).isActive = true
		logoImageView.heightAnchor.constraint(equalTo: bodyView.heightAnchor, multiplier: 0.0545).isActive = true

		let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
		rotation.toValue = Double.pi * 2.0
		rotation.duration = 4.0
		rotation.isCumulative = true
		rotation.repeatCount = Float.greatestFiniteMagnitude
		logoImageView.layer.add(rotation, forKey: "rotationAnimation")

		let timer = Timer.scheduledTimer(timeInterval: 0.5,
										 target: self,
										 selector: #selector(self.update),
										 userInfo: nil,
										 repeats: true)

		DispatchQueue.main.asyncAfter(deadline: .now() + 5.5) {
			timer.invalidate()
			self.logoImageView.layer.removeAllAnimations()

			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
				guard let nav = self.navigationController else { return }

				let homeVC = HomeViewController.build()
				nav.pushViewController(homeVC, animated: false)
			}
		}
	}
	
	@objc func update() {
		scale += 0.02
		logoImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
	}

}
