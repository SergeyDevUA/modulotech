//
//  DetailsTitleCell.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 04.03.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

class DetailsTitleCell: UITableViewCell {

	var titleLabel: UILabel!
	var modeSwitch: UISwitch!
	var textFiled: UITextField!

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		initUI()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func initUI() {
		// Main view
		self.contentView.backgroundColor = .white

		// Title label
		titleLabel = UILabel()
		titleLabel.textAlignment = .left
		titleLabel.font = Style.Font.cellTitle
		titleLabel.textColor = Style.Color.backgroundColor
		contentView.addSubview(titleLabel)

		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0).isActive = true
		titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.65).isActive = true
		titleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 1.0).isActive = true
		
		// Text field
		textFiled = UITextField()
		textFiled.textAlignment = .left
		textFiled.borderStyle = .none
		textFiled.font = Style.Font.cellSubTitle
		textFiled.textColor = .black
		contentView.addSubview(textFiled)
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.textFiled.becomeFirstResponder()
		}

		textFiled.translatesAutoresizingMaskIntoConstraints = false
		textFiled.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:  -65.0).isActive = true
		textFiled.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19.0).isActive = true
		textFiled.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
		textFiled.heightAnchor.constraint(equalToConstant: 27.0).isActive = true
		
		modeSwitch = UISwitch()
		contentView.addSubview(modeSwitch)
		
		modeSwitch.translatesAutoresizingMaskIntoConstraints = false
		modeSwitch.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 13.0).isActive = true
		modeSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -20.0).isActive = true
		
		// TODO: Finish save changes

		//	Separator
		let bottomSeparatorView = UIView()
		bottomSeparatorView.backgroundColor = Style.Color.cellSeparatorColor
		bottomSeparatorView.alpha = 0.3
		contentView.addSubview(bottomSeparatorView)

		bottomSeparatorView.translatesAutoresizingMaskIntoConstraints = false
		bottomSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
		bottomSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
		bottomSeparatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
		bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
	}

}
