//
//  HeaterDeviceTypeViewController.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import UIKit

enum DetailsTypes {
	case title
	case percent
}

struct DetailsItem {
	let title: String
	let mode: Bool
	let value: String
	let type: DetailsTypes
}

class HeaterDeviceTypeViewController: BaseViewController {

	private var headerView: UIView!
	private var tableView: UITableView!
	private var doneButton: UIButton!
	private var viewModel: HeaterDeviceTypeViewModel?
	private let titleReuseIdentifier = "DetailsTitleCell"

	//	Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setup()
	}

}

extension HeaterDeviceTypeViewController {

	static func build(devices: Devices) -> HeaterDeviceTypeViewController {
		let items: [DetailsItem] = [
			DetailsItem(title: devices.deviceName, mode: devices.mode, value:String(devices.temperature ?? 7), type: .title),
		]
		let viewController: HeaterDeviceTypeViewController = HeaterDeviceTypeViewController()
		viewController.viewModel = HeaterDeviceTypeViewModel(devicesItems: items, devices: devices)
		return viewController
	}

}

private extension HeaterDeviceTypeViewController {
	
	func setup() {
		setupHeader()
		setupTableView()
	}
	
	func setupHeader() {
		// Header view container
		headerView = UIView()
		headerView.backgroundColor = Style.Color.backgroundColor
		self.view.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		headerView.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
		headerView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
		headerView.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
		
		// Title label
		let titleLabel = UILabel()
		titleLabel.text = NSLocalizedString("details_title")
		titleLabel.textAlignment = .center
		titleLabel.font = Style.Font.header
		titleLabel.textColor = .white
		headerView.addSubview(titleLabel)
		
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
		titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
		
		// Close button
		let closeButton = UIButton(type: .custom)
		if let image = UIImage(named: "back.png") {
			closeButton.setImage(image, for: .normal)
		}
		closeButton.addTarget(self, action: #selector(closeAction(_:)), for: .touchUpInside)
		headerView.addSubview(closeButton)
		
		closeButton.translatesAutoresizingMaskIntoConstraints = false
		closeButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		closeButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0).isActive = true
		closeButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		closeButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
		
		// Done button
		doneButton = UIButton(type: .custom)
		doneButton.isHidden = true
		if let image = UIImage(named: "done.png") {
			doneButton.setImage(image, for: .normal)
		}
		doneButton.addTarget(self, action: #selector(doneAction(_:)), for: .touchUpInside)
		headerView.addSubview(doneButton)

		doneButton.translatesAutoresizingMaskIntoConstraints = false
		doneButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.0).isActive = true
		doneButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor,constant: -16.0).isActive = true
		doneButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
		doneButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
	}
	
	func setupTableView() {
		let frame = self.view.frame
		tableView = UITableView(frame: frame)
		tableView.separatorStyle = .none
		self.view.addSubview(tableView)
			
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
		tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
		tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
		tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
		
		tableView.register(DetailsTitleCell.self, forCellReuseIdentifier: titleReuseIdentifier)
		tableView.delegate = self
		tableView.dataSource = self
	}
	
	@objc func closeAction(_ sender: UIButton?) {
		backToDeviceList()
	}
	
	@objc func doneAction(_ sender: UIButton?) {
		backToDeviceList()
	}
	
	func backToDeviceList() {
		guard let nav = self.navigationController else { return }
		self.view.endEditing(true)
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
			nav.popViewController(animated: true)
		}
	}

}

extension HeaterDeviceTypeViewController: UITableViewDataSource, UITableViewDelegate {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let userCount = viewModel?.devicesItems.count else { return 0 }

		return userCount
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let item = indexPath.item

		let cell = tableView.dequeueReusableCell(withIdentifier: titleReuseIdentifier, for: indexPath)  as! DetailsTitleCell
		cell.titleLabel.text = viewModel?.devicesItems[item].title
		cell.modeSwitch.isOn = viewModel?.devicesItems[item].mode ?? false
		cell.textFiled.text = viewModel?.devicesItems[item].value
		cell.selectionStyle = .none
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60.0
	}
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
}
