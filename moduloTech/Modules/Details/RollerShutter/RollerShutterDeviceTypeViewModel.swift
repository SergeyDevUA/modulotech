//
//  RollerShutterDeviceTypeViewModel.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 27.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation
import RxSwift

final class RollerShutterDeviceTypeViewModel {

	let devices: Devices?
	let devicesItems: [DetailsItem]

	init(devicesItems: [DetailsItem], devices: Devices) {
		self.devices = devices
		self.devicesItems = devicesItems
	}

}
