//
//  Localized+Simplifications.swift
//  moduloTech
//
//  Created by Sergey Yavorsky on 26.02.2021.
//  Copyright © 2021 Sergey Yavorsky. All rights reserved.
//

import Foundation

func NSLocalizedString(_ key: String) -> String {
    NSLocalizedString(key, comment: "")
}
